package smstest;

import java.rmi.RemoteException;

import org.tempuri.SendSMSSoapProxy;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String mobileNo="078769998";
		String content ="hello test message from axiom MT";
		String endPoint="http://172.16.28.4/sendsms/SendSMS.asmx";
		int timeout=30000;//30s
		
		SendSMSSoapProxy proxy = new SendSMSSoapProxy(endPoint);
		try {
			proxy.setTimeout(timeout);
			String res = proxy.sendSMS(mobileNo, content);
			System.out.println("res from service: " + res);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
