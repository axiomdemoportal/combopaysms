package org.tempuri;

public class SendSMSSoapProxy implements org.tempuri.SendSMSSoap {
  private String _endpoint = null;
  private org.tempuri.SendSMSSoap sendSMSSoap = null;
  
  public SendSMSSoapProxy() {
    _initSendSMSSoapProxy();
  }
  
  public SendSMSSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initSendSMSSoapProxy();
  }
  
  private void _initSendSMSSoapProxy() {
    try {
      sendSMSSoap = (new org.tempuri.SendSMS_ServiceLocator()).getSendSMSSoap();
      if (sendSMSSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)sendSMSSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)sendSMSSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setTimeout(int timeout)
  {
	  SendSMSSoap soap = getSendSMSSoap();
	  ((SendSMSSoapStub)soap).setTimeout(timeout);
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (sendSMSSoap != null)
      ((javax.xml.rpc.Stub)sendSMSSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.SendSMSSoap getSendSMSSoap() {
    if (sendSMSSoap == null)
      _initSendSMSSoapProxy();
    return sendSMSSoap;
  }
  
  public java.lang.String sendSMS(java.lang.String sMobileNo, java.lang.String sSMSContent) throws java.rmi.RemoteException{
    if (sendSMSSoap == null)
      _initSendSMSSoapProxy();
    return sendSMSSoap.sendSMS(sMobileNo, sSMSContent);
  }
  
  
}