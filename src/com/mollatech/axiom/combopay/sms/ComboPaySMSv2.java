package com.mollatech.axiom.combopay.sms;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.communication.MobileConnectorSettings;
import com.mollatech.axiom.connector.communication.SMSSender;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.security.MessageDigest;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.tempuri.SendSMSSoapProxy;

public class ComboPaySMSv2 implements SMSSender {

    static MobileConnectorSettings smsSetting = null;
    static String smsIpAddress;
    static String smsUserId = null;
    static String smsPassword = null;
    static String smsPhonenumber = null;
    static String keyWordName = null;
    static int smsPort = 80;
    final String FAILED = "FAILED";
    final String SENT = "SENT";
    int result = -1;

    @Override
    public AXIOMStatus Load(MobileConnectorSettings setting) {

        AXIOMStatus axiomstatus = new AXIOMStatus();

        smsSetting = setting;
        if (smsSetting != null) {
            smsUserId = smsSetting.userid;
            smsPassword = smsSetting.password;
            smsPhonenumber = smsSetting.phoneNumber;
            smsIpAddress = smsSetting.ip;
            smsPort = smsSetting.port;
            keyWordName = (String) smsSetting.reserve1;
            axiomstatus.iStatus = 0;
            axiomstatus.strStatus = "Loaded";
            smsSetting = null;
            return axiomstatus;
        }
        return null;
    }

    @Override
    public AXIOMStatus Unload() {
        AXIOMStatus axiomstatus = new AXIOMStatus();

        if (smsSetting != null) {
            axiomstatus.iStatus = 0;
            axiomstatus.strStatus = "unloaded";
            return axiomstatus;

        }

        axiomstatus.iStatus = 1;
        axiomstatus.strStatus = "error";
        return axiomstatus;
    }

    @Override
    public String SendMessage(String number, String message) {
        String sid = null;
        try {

            String destination = number;
//            String sendFrom = smsPhonenumber;
//            String keywordName = keyWordName;
//            String outContent = message;
//            String chargingFlag = "0";
//            String moSeqNo = "0";
//            String totalMessage = "1";
//            String contentType = "0";
//            String secretCode = smsPassword;
            String ipAddress = smsIpAddress;
            int port = smsPort;
            Date d = new Date();
            String msgid = number + d.toString();
            byte[] SHA1hash = SHA1(msgid);
            sid = asHex(SHA1hash);

            // TODO Auto-generated method stub
            String mobileNo = destination;
            String content = message;
            //String endPoint = "http://172.16.28.4/sendsms/SendSMS.asmx";
            String endPoint = "http://" + ipAddress + ":" + port + "/sendsms/SendSMS.asmx";
            int timeout = 30000;//30s

            SendSMSSoapProxy proxy = new SendSMSSoapProxy(endPoint);
            try {
                proxy.setTimeout(timeout);
                String res;
                res = proxy.sendSMS(mobileNo, content);
                System.out.println("Result from Service: " + res);
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//            //  URL url = new URL("http://" + ipAddress + ":" + port + "/AcledaSMS/MTSend.asmx?WSDL");
//            //  MTSend mtSend = new 
//            URL url = new URL("http://" + ipAddress + "/AcledaSMS/MTSend.asmx?WSDL");
//
//            vn.vnpay.vascontent.MTSend service = new vn.vnpay.vascontent.MTSend(url);
//            vn.vnpay.vascontent.MTSendSoap mtPort = service.getMTSendSoap();
//            int result = mtPort.sendMT(destination, sendFrom, keywordName, outContent, chargingFlag, moSeqNo, totalMessage, contentType, secretCode);
//            // sendMT(Destination, SendFrom, KeywordName, OutContent, ChargingFlag, MOSeqNo, TotalMessage, ContentType, SecretCode);
//
//            if (result == 0) {
//                this.result = 0;
//            }
            return sid;
        } catch (Exception ex) {
            Logger.getLogger(ComboPaySMSv2.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sid;

    }

    @Override
    public AXIOMStatus GetStatus(String msgid) {

        AXIOMStatus aStatus = new AXIOMStatus();
        aStatus.iStatus = -1;
        aStatus.strStatus = "failed";
        try {

            if (this.result == 0) {
                aStatus.iStatus = 0;
                aStatus.strStatus = "completed";
            } else {
                return aStatus;
            }
            return aStatus;
        } catch (Exception e) {
            return aStatus;

        }

    }

    private byte[] SHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return array;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public AXIOMStatus GetServiceStatus() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;

        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10) {
                strbuf.append("0");
            }

            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }

        return strbuf.toString();
    }

}
