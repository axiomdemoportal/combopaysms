/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.mollatech.axiom.connector.communication.AXIOMStatus
 *  com.mollatech.axiom.connector.communication.MobileConnectorSettings
 *  com.mollatech.axiom.connector.communication.SMSSender
 */
package com.mollatech.axiom.combopay.sms;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.communication.MobileConnectorSettings;
import com.mollatech.axiom.connector.communication.SMSSender;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Date;
import vn.vnpay.vascontent.MTSend;
import vn.vnpay.vascontent.MTSendSoap;

public class ComboPaySMS
        implements SMSSender {

    static MobileConnectorSettings smsSetting = null;
    static String smsIpAddress;
    static String smsUserId;
    static String smsPassword;
    static String smsPhonenumber;
    static String keyWordName;
    static int smsPort;
    final String FAILED = "FAILED";
    final String SENT = "SENT";
    int result = -1;
//    25-01-2017

    public static MTSendSoap mtPort;

    public AXIOMStatus Load(MobileConnectorSettings setting) {
        AXIOMStatus axiomstatus = new AXIOMStatus();
        smsSetting = setting;
        if (smsSetting != null) {
            smsUserId = ComboPaySMS.smsSetting.userid;
            smsPassword = ComboPaySMS.smsSetting.password;
            smsPhonenumber = ComboPaySMS.smsSetting.phoneNumber;
            smsIpAddress = ComboPaySMS.smsSetting.ip;
            smsPort = ComboPaySMS.smsSetting.port;
            keyWordName = (String) ComboPaySMS.smsSetting.reserve1;
            axiomstatus.iStatus = 0;
            axiomstatus.strStatus = "Loaded";
            smsSetting = null;
            URL url = null;
            try {
                url = new URL("http://" + smsIpAddress + "/AcledaSMS/MTSend.asmx?WSDL");
                MTSend service = new MTSend(url);
                mtPort = service.getMTSendSoap();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return axiomstatus;
        }
        return null;
    }

    public AXIOMStatus Unload() {
        AXIOMStatus axiomstatus = new AXIOMStatus();
        if (smsSetting != null) {
            axiomstatus.iStatus = 0;
            axiomstatus.strStatus = "unloaded";
            return axiomstatus;
        }
        axiomstatus.iStatus = 1;
        axiomstatus.strStatus = "error";
        return axiomstatus;
    }

    public String SendMessage(String number, String message) {
        String sid = null;
        String destination = number;
        String sendFrom = smsPhonenumber;
        String keywordName = keyWordName;
        String outContent = message;
        String chargingFlag = "0";
        String moSeqNo = "0";
        String totalMessage = "1";
        String contentType = "0";
        String secretCode = smsPassword;
        int port = smsPort;
        Date d = new Date();
        String msgid = number + d.toString();
        byte[] SHA1hash = this.SHA1(msgid);
        sid = this.asHex(SHA1hash);
        if (mtPort == null) {
            URL url = null;
            try {
                url = new URL("http://" + smsIpAddress + "/AcledaSMS/MTSend.asmx?WSDL");
                MTSend service = new MTSend(url);
                mtPort = service.getMTSendSoap();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        SmsThread thread = new SmsThread(mtPort, destination, sendFrom, keywordName, outContent, chargingFlag, moSeqNo, totalMessage, contentType, secretCode);
        thread.run();
        this.result = 0;
        return sid;
    }

    public AXIOMStatus GetStatus(String msgid) {
        AXIOMStatus aStatus = new AXIOMStatus();
        aStatus.iStatus = -1;
        aStatus.strStatus = "failed";
        try {
            if (this.result != 0) {
                return aStatus;
            }
            aStatus.iStatus = 0;
            aStatus.strStatus = "completed";
            return aStatus;
        } catch (Exception e) {
            return aStatus;
        }
    }

    private byte[] SHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return array;
        } catch (Exception e) {
            return null;
        }
    }

    public AXIOMStatus GetServiceStatus() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String asHex(byte[] buf) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        for (int i = 0; i < buf.length; ++i) {
            if ((buf[i] & 255) < 16) {
                strbuf.append("0");
            }
            strbuf.append(Long.toString(buf[i] & 255, 16));
        }
        return strbuf.toString();
    }

    static {
        smsUserId = null;
        smsPassword = null;
        smsPhonenumber = null;
        keyWordName = null;
        smsPort = 80;
    }
}
