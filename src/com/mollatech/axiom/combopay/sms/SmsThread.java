/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.combopay.sms;

import java.util.Date;
import vn.vnpay.vascontent.MTSendSoap;

/**
 *
 * @author pramodchaudhari
 */
public class SmsThread implements Runnable {

    public MTSendSoap m_portT;
    String destinationT, sendFromT, keywordNameT, outContentT, chargingFlagT, moSeqNoT, totalMessageT, contentTypeT, secretCodeT;

    public SmsThread(MTSendSoap m_port, String destination, String sendFrom, String keywordName, String outContent, String chargingFlag, String moSeqNo, String totalMessage, String contentType, String secretCode) {
        destinationT = destination;
        sendFromT = sendFrom;
        keywordNameT = keywordName;
        outContentT = outContent;
        chargingFlagT = chargingFlag;
        moSeqNoT = moSeqNo;
        totalMessageT = totalMessage;
        contentTypeT = contentType;
        secretCodeT = secretCode;
        m_portT = m_port;
    }
    @Override
    public void run() {
     System.out.println("Sms sending start to :"+destinationT+"@Time"+new Date());
     int result=   m_portT.sendMT(destinationT, sendFromT, keywordNameT, outContentT, chargingFlagT, moSeqNoT, totalMessageT, contentTypeT, secretCodeT);
        System.out.println("Sms sent to  :"+destinationT+"With result :"+result +"@Time"+new Date());
    
    }
}
